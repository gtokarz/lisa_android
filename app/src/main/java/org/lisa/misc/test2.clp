(deftemplate  zadanie_status ()
 (slot value (default null)))
(deftemplate  mozliwe_nowe_statusy ()
 (slot value (default null)))
(deftemplate  osoba ()
 (slot value (default null)))

(defrule mozliweprzejsciastatusow/1 ""
?_osoba <- (osoba {value == specjalista})
?_zadanie_status <- (zadanie_status (value ?v $ :(member$ ?v (create$  nowe  zwrocone ))))
=>
  (assert (mozliwe_nowe_statusy (value [w_realizacji])))
  (printout t "(assert (mozliwe_nowe_statusy (value [w_realizacji])))" crlf)
)
(defrule mozliweprzejsciastatusow/2 ""
?_osoba <- (osoba {value == specjalista})
?_zadanie_status <- (zadanie_status (value ?v $ :(member$ ?v (create$  w_realizacji ))))
=>
  (assert (mozliwe_nowe_statusy (value [zrealizowane])))
  (printout t "(assert (mozliwe_nowe_statusy (value [zrealizowane])))" crlf)
)
(defrule mozliweprzejsciastatusow/3 ""
?_osoba <- (osoba (value ?v $ :(not (member$ ?v (create$  specjalista )))))
?_zadanie_status <- (zadanie_status (value ?v $ :(member$ ?v (create$  w_realizacji ))))
=>
  (assert (mozliwe_nowe_statusy (value [brak])))
  (printout t "(assert (mozliwe_nowe_statusy (value [brak])))" crlf)
)
(defrule mozliweprzejsciastatusow/4 ""
?_osoba <- (osoba {value == kierownik_projektu})
?_zadanie_status <- (zadanie_status (value ?v $ :(member$ ?v (create$  brak ))))
=>
  (assert (mozliwe_nowe_statusy (value [nowe])))
  (printout t "(assert (mozliwe_nowe_statusy (value [nowe])))" crlf)
)
(defrule mozliweprzejsciastatusow/5 ""
?_osoba <- (osoba {value == kierownik_projektu})
?_zadanie_status <- (zadanie_status (value ?v $ :(member$ ?v (create$  zrealizowane ))))
=>
  (assert (mozliwe_nowe_statusy (value [odebrane ,zwrocone])))
  (printout t "(assert (mozliwe_nowe_statusy (value [odebrane ,zwrocone])))" crlf)
)
(defrule startup "" 
=>
;DATA
)
(watch rules all) 
 (reset) 
 (run)
