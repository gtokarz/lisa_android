;rewrite lisa.asd to allow loading from jar/apk
(require :asdf)
; ((:module packages
; :components
; ((:file "pkgdecl")))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/packages/pkgdecl.lisp"))
; (:module utils
; :components
; ((:file "compose")
 ; (:file "utils"))
; :serial t)
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/utils/compose.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/utils/utils.lisp"))
; (:module belief-systems
                        ; :components
                        ; ((:file "belief")
                         ; (:file "certainty-factors"))
                        ; :serial t)
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/belief-systems/belief.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/belief-systems/certainty-factors.lisp"))
; (:module reflect
                        ; :components
                        ; ((:file "reflect")))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/reflect/reflect.lisp"))
               ; (:module core
                        ; :components
                        ; ((:file "preamble")
                         ; (:file "conditions")
                         ; (:file "deffacts")
                         ; (:file "fact")
                         ; (:file "watches")
                         ; (:file "activation")
                         ; (:file "heap")
                         ; (:file "conflict-resolution-strategies")
                         ; (:file "context")
                         ; (:file "rule")
                         ; (:file "pattern")
                         ; (:file "rule-parser")
                         ; (:file "fact-parser")
                         ; (:file "language")
                         ; (:file "tms-support")
                         ; (:file "rete")
                         ; (:file "belief-interface")
                         ; (:file "meta")
                         ; (:file "binding")
                         ; (:file "token")
                         ; (:file "retrieve"))
                        ; :serial t)
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/preamble.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/conditions.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/deffacts.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/fact.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/watches.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/activation.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/heap.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/conflict-resolution-strategies.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/context.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/rule.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/pattern.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/rule-parser.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/fact-parser.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/language.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/tms-support.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/rete.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/belief-interface.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/meta.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/binding.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/token.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/core/retrieve.lisp"))
               ; (:module implementations
                        ; :components
                        ; ((:file "workarounds")
                         ; #+:lispworks
                         ; (:file "lispworks-auto-notify")
                         ; #+:cmucl
                         ; (:file "cmucl-auto-notify")
                         ; #+:allegro
                         ; (:file "allegro-auto-notify"))
                        ; :serial t)
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/implementations/workarounds.lisp"))
               ; (:module rete
                        ; :pathname "rete/reference/"
                        ; :components
                        ; ((:file "node-tests")
                         ; (:file "shared-node")
                         ; (:file "successor")
                         ; (:file "node-pair")
                         ; (:file "terminal-node")
                         ; (:file "node1")
                         ; (:file "join-node")
                         ; (:file "node2")
                         ; (:file "node2-not")
                         ; (:file "node2-test")
                         ; (:file "node2-exists")
                         ; (:file "rete-compiler")
                         ; (:file "tms")
                         ; (:file "network-ops")
                         ; (:file "network-crawler"))
                        ; :serial t)
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/node-tests.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/shared-node.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/successor.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/node-pair.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/terminal-node.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/node1.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/join-node.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/node2.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/node2-not.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/node2-test.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/node2-exists.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/rete-compiler.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/tms.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/network-ops.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/rete/reference/network-crawler.lisp"))
               ; (:module config
                        ; :components
                        ; ((:file "config")
                         ; (:file "epilogue"))
                        ; :serial t))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/config/config.lisp"))
(system::load-system-file (concatenate 'string CL-USER::lisa-base "/org/lisa/src/config/epilogue.lisp"))
; ((:module src
              ; :components
              
               
               
               
              ; :serial t))

