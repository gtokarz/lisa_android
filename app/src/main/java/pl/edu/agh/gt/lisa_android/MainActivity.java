package pl.edu.agh.gt.lisa_android;

import android.os.AsyncTask;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import org.armedbear.lisp.Interpreter;
import org.armedbear.lisp.LispObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static List<Long> memoryUsage = new LinkedList<>();
    private static List<Float> processorUsage = new LinkedList<>();
    private static boolean ifContinue = true;
    private Date start, end;
    private BufferedWriter bw;
    public class Monitor extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            while (ifContinue) {
                memoryUsage.add(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
                processorUsage.add(readUsage());
            }
            System.out.println("MONITOR: KONIEC");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            if(bw!=null)
                return;
            Long [] mem = memoryUsage.toArray(new Long[memoryUsage.size()]);
            long minMem = mem[0];
            long maxMem = mem[0];
            for (long m : mem) {
                if (m < minMem) minMem = m;
                if (m > maxMem) maxMem = m;
            }

            Float [] proc = processorUsage.toArray(new Float[processorUsage.size()]);
            float minProc = proc[0];
            float maxProc = proc[0];
            for (float p : proc) {
                if (p < minProc) minProc = p;
                if (p > maxProc) maxProc = p;
            }
            try {
                bw.write(printDateTime());
                bw.write("Mem diff: ");
                bw.write(String.valueOf( (maxMem - minMem) / 1024L));
                bw.write(" KB , Processor diff: ");
                bw.write(String.valueOf( (maxProc-minProc)*100));
                bw.write(" % , Time diff: ");
                bw.write(String.valueOf(end.getTime() - start.getTime()));
                bw.write(" ms \n-------\n");
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }

    private String printDateTime() {
        Calendar c = Calendar.getInstance();
        return String.format("%tY-%tm-%td %tH:%tM:%tS : ", c, c, c, c, c, c);
    }

    private float readUsage() {
        try {
            RandomAccessFile reader = new RandomAccessFile("/proc/stat", "r");
            String load = reader.readLine();

            String[] toks = load.split(" +");  // Split on one or more spaces

            long idle1 = Long.parseLong(toks[4]);
            long cpu1 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            try {
                Thread.sleep(360);
            } catch (Exception ignored) {}

            reader.seek(0);
            load = reader.readLine();
            reader.close();

            toks = load.split(" +");

            long idle2 = Long.parseLong(toks[4]);
            long cpu2 = Long.parseLong(toks[2]) + Long.parseLong(toks[3]) + Long.parseLong(toks[5])
                    + Long.parseLong(toks[6]) + Long.parseLong(toks[7]) + Long.parseLong(toks[8]);

            return (float)(cpu2 - cpu1) / ((cpu2 + idle2) - (cpu1 + idle1));

        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return 0;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            File lisaBench = new File(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/benchmark");
            lisaBench.mkdirs();
            bw = new BufferedWriter(new FileWriter(Environment.getExternalStorageDirectory()
                    .getAbsolutePath()+"/benchmark/lisa.txt",true));
        } catch (IOException e) {
            //e.printStackTrace();
        }
        System.out.print("Is lisp here: ");
        Interpreter interpreter = null;
        try
        {
            interpreter = Interpreter.createInstance();
            if (interpreter == null)
                interpreter = Interpreter.getInstance();

            if (interpreter != null)
                System.out.println(" true");
            /**
             * Armored Bear Hello
             */
            interpreter.eval("(defun HELLO () " +
                    "\"HELLO FROM ANDROID(OTHER SIDE)\"" +
                    ")");
            LispObject helloFunc = interpreter.eval("(HELLO)");
            System.out.println("Lisp says: " +helloFunc);
            //interpreter.dispose();
        }
        catch (Throwable t)
        {
            System.out.println("exception!");
            t.printStackTrace();
        }
        //;ustawiamy tutaj relatywna sciezke do root apk względem org.armoredbear.lisp
        interpreter.eval("(setf CL-USER::lisa-base \""+Environment.getExternalStorageDirectory()
                .getAbsolutePath()+"/LisaSystem\")");
        //boot lisa subsystem
        //interpreter.eval("(system::load-system-file \"jvm\")");
        //System.out.println("BOOTING LISA:" + interpreter.eval("(system::load (concatenate 'string CL-USER::lisa-base \"/boot-lisa\"))"));
        //System.out.println(interpreter.eval("(system::load )"));
        //System.out.println(interpreter.eval("(compile-file (concatenate 'string CL-USER::lisa-base \"/org/lisa/misc/mab.lisp\"))"));
        //System.out.println(interpreter.eval("(LISA-USER::run-mab)"));
        interpreter.eval("(require :asdf)");
        interpreter.eval("(system::load (concatenate 'string CL-USER::lisa-base \"/org/lisa/lisa.asd\"))");
        interpreter.eval("(asdf::compile-system :lisa)");
        //interpreter.eval("(system::compile-system (concatenate 'string CL-USER::lisa-base \"/boot-lisa\"))");
        //System.out.println(interpreter.eval("(system::load-system-file \"etest\")"));
        //measuring block
        ifContinue = true;
        new Monitor().execute();
        start = new Date();
        //measuring block

        end = new Date();
        ifContinue = false;
    }
}
